package com.greatlearning.stockers;

import com.greatlearning.stockers.controller.StockController;

public class StockApp {

    public static void main(String[] args){
        StockController stockController = new StockController();
        stockController.run();
        stockController.doService();
    }


}
