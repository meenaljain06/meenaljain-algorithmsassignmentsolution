package com.greatlearning.stockers.controller;

import com.greatlearning.stockers.model.Stock;
import com.greatlearning.stockers.service.StockService;

import java.util.Scanner;

public class StockController {

    int noOfCompanies;
    Stock[] stocks;

    public void run(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of companies");
        this.noOfCompanies = scanner.nextInt();
        this.stocks = new Stock[this.noOfCompanies];

        for(int i=0; i<this.noOfCompanies; ++i){
            Stock stock = new Stock();
            System.out.println(" Enter the current stock price of the company" +(i+1) +": ");
            stock.setStockPrice(scanner.nextDouble());
            System.out.println("Whether company's stock price rose today as compared to yesterday's? (true/false)");
            stock.setPriceIncreased(scanner.nextBoolean());
            this.stocks[i] = stock;
        }
    }

    public int showMenu(Scanner scanner){
        System.out.println("Enter the operation that you want to perform");
        System.out.println("1. Display the companies stock price in ascending order");
        System.out.println("2. Display the companies stock price in descending order");
        System.out.println("3. Display the total number of companies for which the stock price rose today");
        System.out.println("4. Display the total number of companies for which the stock price declined today");
        System.out.println("5. Search a specific stock price");
        System.out.println("Press 0 to exit");
        System.out.println("Enter your choice");
        int choice = scanner.nextInt();
        return  choice;
    }

    public void doService(){
        StockService stockService = new StockService();
        Scanner scanner = new Scanner(System.in);
        int choice;
        do{
            choice = this.showMenu(scanner);
            switch (choice){
                case 0:
                    System.out.println("Exited successfully");
                    break;
                case 1:
                    stockService.displayStockInAscendingOrder(this.stocks, this.noOfCompanies);
                    break;
                case 2:
                    stockService.displayStocksInDescendingOrder(this.stocks, this.noOfCompanies);
                    break;
                case 3:
                    System.out.println(stockService.showStockRiseCount(this.stocks,this.noOfCompanies));
                    break;
                case 4:
                    System.out.println(stockService.showStockDeclineCount(this.stocks, this.noOfCompanies));
                    break;
                case 5:
                    System.out.println("Enter value to be searched: ");
                    double searchVal = scanner.nextDouble();
                    if(stockService.searchStockPrice(this.stocks,this.noOfCompanies,searchVal)){
                        System.out.println("Stock of value " + searchVal + " is present.");
                    } else {
                        System.out.println("Stock of value " +searchVal + " is not found.");
                    }
                    break;
                default:
                    System.out.println("You entered invalid choice");
            }
        }while(choice != 0);
    }
}
