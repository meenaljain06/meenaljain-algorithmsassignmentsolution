package com.greatlearning.stockers.model;

public class Stock {

    private double stockPrice;
    private boolean priceIncreased;

    public double getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(double stockPrice) {
        this.stockPrice = stockPrice;
    }

    public boolean isPriceIncreased() {
        return priceIncreased;
    }

    public void setPriceIncreased(boolean priceIncreased) {
        this.priceIncreased = priceIncreased;
    }
}
