package com.greatlearning.stockers.service;

import com.greatlearning.stockers.model.Stock;

import java.util.Arrays;

public class StockService {

    Double[] stockPrices;

    public void displayStockInAscendingOrder(Stock[] stocks, int noOfCompanies){
        this.stockPrices = new Double[noOfCompanies];
        for(int i=0; i<noOfCompanies; ++i){
            this.stockPrices[i] = stocks[i].getStockPrice();
        }
        Arrays.sort(this.stockPrices);
        System.out.println("Stock values in ascending order: ");
        for(int i=0; i<noOfCompanies; ++i){
            System.out.println(this.stockPrices[i] + " ");
        }
    }

    public void displayStocksInDescendingOrder(Stock[] stocks, int noOfCompanies){
        this.stockPrices = new Double[noOfCompanies];
        for(int i=0; i<noOfCompanies; ++i){
            this.stockPrices[i] = stocks[i].getStockPrice();
        }

        Arrays.sort(this.stockPrices);

        System.out.println("Stock values in descending order: ");
        for(int i=noOfCompanies-1; i>-1; --i ){
            System.out.println(this.stockPrices[i] + " ");
        }
    }

    public int showStockRiseCount(Stock[] stocks, int noOfCompanies){
        int count=0;

        for(int i=0; i<noOfCompanies; ++i){
            if(stocks[i].isPriceIncreased()){
                ++count;
            }
        }

        return count;
    }

    public int showStockDeclineCount(Stock[] stocks, int noOfCompanies){
        int count = 0;

        for(int i=0; i<noOfCompanies; ++i){
            if(!stocks[i].isPriceIncreased()){
                ++count;
            }
        }
        return count;
    }

    public boolean searchStockPrice(Stock[] stocks, int noOfCompanies, double searchStockPrice){
        for(int i=0; i<noOfCompanies; ++i){
            if(stocks[i].getStockPrice() == searchStockPrice){
                return true;
            }
        }
        return false;
    }
}
